import React from 'react'
import { Switch, Route, Redirect } from 'react-router'

import Home from '../components/home/Home'
import Login from '../components/users/Login'
import UserCrud from '../components/users/UserCrud'
import User from '../components/users/User'


export default props =>
	<Switch>
		
		<Route exact path='/' component={Home} />
		<Route  path='/login' component={Login} />
		<Route  path='/users' component={UserCrud} />
		<Route  path='/user' component={User} />
		<Redirect from='*' to='/' />
		
	</Switch>