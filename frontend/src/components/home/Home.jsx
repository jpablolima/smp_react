import React from 'react'
import Main from '../template/Main'

export default props =>
	<Main icon="home" title="Início" 
			subtitle="Sistema SMP em React">
			<div className='display-4'> Bem Vindo</div>
			<hr />
			<p className="mb-0"> Sistema para crição de Cadastro e Monitoração de 
			Proposição em React</p>

	</Main>