import React, { Component } from 'react'
import axios from 'axios'
import Main from '../template/Main'

const headerProps = {
    icon: 'users',
    title: 'Proposição',
    subtitle: 'Cadastro de Proposição: Incluir, Listar, Alterar e Excluir!'
}

const baseUrl = 'http://localhost:3001/users'
const initialState = {
    user: { proposicao: '', ano: '', autor:'', ementa:'', apresentacao:''  },
    list: []
}

export default class UserCrud extends Component {

    state = { ...initialState }

    componentWillMount() {
        axios(baseUrl).then(resp => {
            this.setState({ list: resp.data })
        })
    }

    clear() {
        this.setState({ user: initialState.user })
    }

    save() {
        const user = this.state.user
        const method = user.id ? 'put' : 'post'
        const url = user.id ? `${baseUrl}/${user.id}` : baseUrl
        axios[method](url, user)
            .then(resp => {
                const list = this.getUpdatedList(resp.data)
                this.setState({ user: initialState.user, list })
            })
    }

    getUpdatedList(user, add = true) {
        const list = this.state.list.filter(u => u.id !== user.id)
        if(add) list.unshift(user)
        return list
    }

    updateField(event) {
        const user = { ...this.state.user }
        user[event.target.name] = event.target.value
        this.setState({ user })
    }

    renderForm() {
        return (
            <div className="form">
                <div className="row">
                    <div className="col-12 col-md-6">
                        <div className="form-group">
                            <label>Proposição</label>
                            <input type="text" className="form-control"
                                name="proposicao"
                                value={this.state.user.user}
                                onChange={e => this.updateField(e)}
                                placeholder="Digite a Proposição" />
                        </div>
                    </div>

                    <div className="col-12 col-md-6">
                        <div className="form-group">
                            <label>Ano</label>
                            <input type="text" className="form-control"
                                name="ano"
                                value={this.state.user.email}
                                onChange={e => this.updateField(e)}
                                placeholder="Ano" />
                        </div>
                    </div>
                       <div className="col-12 col-md-6">
                        <div className="form-group">
                            <label>Autor</label>
                            <input type="text" className="form-control"
                                name="autor"
                                value={this.state.user.email}
                                onChange={e => this.updateField(e)}
                                placeholder="Autor" />
                        </div>
                    </div>
                       <div className="col-12 col-md-6">
                        <div className="form-group">
                            <label>Ementa</label>
                            <input type="text" className="form-control"
                                name="ementa"
                                value={this.state.user.email}
                                onChange={e => this.updateField(e)}
                                placeholder="Ementa" />
                        </div>
                    </div>
                           <div className="col-12 col-md-6">
                            <div className="form-group">
                            <label>Data de Apresentação</label>
                            <input type="text" className="form-control"
                                name="apresentacao"
                                value={this.state.user.email}
                                onChange={e => this.updateField(e)}
                                placeholder="Apresentacao" />
                        </div>
                    </div>

                </div>

                <hr />
                <div className="row">
                    <div className="col-12 d-flex justify-content-end">
                        <button className="btn btn-primary"
                            onClick={e => this.save(e)}>
                            Salvar
                        </button>

                        <button className="btn btn-secondary ml-2"
                            onClick={e => this.clear(e)}>
                            Cancelar
                        </button>
                    </div>
                </div>
            </div>
        )
    }

    load(user) {
        this.setState({ user })
    }

    remove(user) {
        axios.delete(`${baseUrl}/${user.id}`).then(resp => {
            const list = this.getUpdatedList(user, false)
            this.setState({ list })
        })
    }

    renderTable() {
        return (
            <table className="table mt-4">
                <thead>
                    <tr>
                        <th>Proposição</th>
                        <th>Ano</th>
                        <th>Autor</th>
                        <th>Ementa</th>
                        <th>Apresentação</th>
                    </tr>
                </thead>
                <tbody>
                    {this.renderRows()}
                </tbody>
            </table>
        )
    }

    renderRows() {
        return this.state.list.map(user => {
            return (
                <tr key={user.id}>
                    <td>{user.proposicao}</td>
                    <td>{user.ano}</td>
                    <td>{user.autor}</td>
                    <td>{user.ementa}</td>
                    <td>{user.apresentacao}</td>
                
                    <td>
                        <button className="btn btn-warning "
                            onClick={() => this.load(user)}>
                            <i className="fa fa-pencil"></i>
                        </button>
                        <button className="btn btn-danger"
                            onClick={() => this.remove(user)}>
                            <i className="fa fa-trash"></i>
                        </button>

                        <button className="btn btn-info ml-2">
                           <i className="fa fa-envelope"></i>
                        </button>

                    </td>
                </tr>
            )
        })
    }
    
    render() {
        return (
            <Main {...headerProps}>
                {this.renderForm()}
                {this.renderTable()}
            </Main>
        )
    }
}